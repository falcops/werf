import os
from flask import Flask

app = Flask(__name__)
CI_COMMIT_SHORT_SHA = os.environ.get("CI_COMMIT_SHORT_SHA", "xxx")
CI_COMMIT_REF_NAME = os.environ.get("CI_COMMIT_REF_NAME", "xxx")
CI_ENVIRONMENT_URL = os.environ.get("CI_ENVIRONMENT_URL", "localhost")

@app.route("/")
def hello_world():
    return {"url": CI_ENVIRONMENT_URL, "branch": CI_COMMIT_REF_NAME, "commit": CI_COMMIT_SHORT_SHA}
